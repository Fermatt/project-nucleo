﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class singleton : MonoBehaviour {

    public static singleton instance;

	// Use this for initialization
	void Awake () {
        if (!instance)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
