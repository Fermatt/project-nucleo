﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDialogue : MonoBehaviour {

    [SerializeField]
    private DialogueData data;
    [SerializeField] private GameObject npc;

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag == "Player")
        {
            DialogueManager.instance.StartDialogue(data);
            if(npc != null)
            {
                GameManager.instance.talkingNPC = npc;
            }
            Destroy(gameObject);
        }
    }
}
