﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CVTextPersonalidad : MonoBehaviour {

    private string ultimoNombre;
    public Text txt;
    private string[] Nombre;
    void Start()
    {

    }

    // Update is called once per frame + "\n"+ "hola" txt.text =
    void Update()
    {
        Nombre = new string[5];
        texto(ultimoNombre);
    }

    private void OnEnable()
    {
        texto(ultimoNombre);
    }

    public void texto(string nombre)
    {
        ultimoNombre = nombre;

        if (nombre == "Dina")
        {
            if (GameManager.instance.CVname[1][5] == false)
            {
                Nombre[0] = "█████████";
            }
            else
            {
                Nombre[0] = "Timida";
            }
            if (GameManager.instance.CVname[1][6] == false)
            {
                Nombre[1] = "█████████";
            }
            else
            {
                Nombre[1] = "Positiva";
            }
            txt.text = Nombre [0] + "\n" + Nombre [1];
        }
        if (nombre == "Eugene")
        {
            if (GameManager.instance.CVname[2][5] == false)
            {
                Nombre[0] = "█████████";
            }
            else
            {
                Nombre[0] = "Introvertido";
            }
            if (GameManager.instance.CVname[2][6] == false)
            {
                Nombre[1] = "█████████";
            }
            else
            {
                Nombre[1] = "Callado";
            }
            txt.text = Nombre[0] + "\n" + Nombre[1];
        }
        if (nombre == "Christie")
        {
            if (GameManager.instance.CVname[3][5] == false)
            {
                Nombre[0] = "█████████";
            }
            else
            {
                Nombre[0] = "Arrogante";
            }
            if (GameManager.instance.CVname[3][6] == false)
            {
                Nombre[1] = "█████████ █████████ █████████ █████████";
            }
            else
            {
                Nombre[1] = "Trastorno histriónico de la personalidad";
            }
            txt.text = Nombre[0] + "\n" + Nombre[1];
        }
        if (nombre == "Hammond")
        {
            if (GameManager.instance.CVname[4][5] == false)
            {
                Nombre[0] = "█████████";
            }
            else
            {
                Nombre[0] = "Responsable";
            }
            if (GameManager.instance.CVname[4][6] == false)
            {
                Nombre[1] = "█████████";
            }
            else
            {
                Nombre[1] = "Servicial";
            }
            txt.text = Nombre[0] + "\n" + Nombre[1];
        }
        if (nombre == "Blake")
        {
            if (GameManager.instance.CVname[5][5] == false)
            {
                Nombre[0] = "█████████";
            }
            else
            {
                Nombre[0] = "Carismatico";
            }
            if (GameManager.instance.CVname[5][6] == false)
            {
                Nombre[1] = "█████████";
            }
            else
            {
                Nombre[1] = "paciente";
            }
            txt.text = Nombre[0] + "\n" + Nombre[1];
        }
        if (nombre == "Frank")
        {
            if (GameManager.instance.CVname[6][5] == false)
            {
                Nombre[0] = "█████████";
            }
            else
            {
                Nombre[0] = "Confiable";
            }
            if (GameManager.instance.CVname[6][6] == false)
            {
                Nombre[1] = "█████████";
            }
            else
            {
                Nombre[1] = "Lider";
            }
            txt.text = Nombre[0] + "\n" + Nombre[1];
        }
        if (nombre == "William")
        {
            if (GameManager.instance.CVname[7][5] == false)
            {
                Nombre[0] = "█████████";
            }
            else
            {
                Nombre[0] = "antipatico";
            }
            if (GameManager.instance.CVname[7][6] == false)
            {
                Nombre[1] = "█████████";
            }
            else
            {
                Nombre[1] = "callado";
            }
            txt.text = Nombre[0] + "\n" + Nombre[1];
        }

        if(nombre == "-")
        {
            txt.text = "";
        }

    }
}
