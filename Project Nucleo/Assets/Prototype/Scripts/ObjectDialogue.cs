﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDialogue : MonoBehaviour {

    [SerializeField]
    private DialogueData data;

    void OnTriggerStay2D(Collider2D c)
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            DialogueManager.instance.StartDialogue(data);
        }
    }
}
