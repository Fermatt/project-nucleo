﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footsteps2 : MonoBehaviour {

    private float timer = 0;
    [SerializeField] private float delay;
    [SerializeField] private AudioClip[] m_FootstepSounds;

    private AudioSource m_AudioSource;

    // Use this for initialization
    void Start () {
        m_AudioSource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        float i = Input.GetAxis("Vertical");
        float k = Input.GetAxis("Horizontal");
        if ((k != 0 || i != 0) && GameManager.instance.gameState == "Play")
        {
            timer += Time.deltaTime;
            if (timer > delay)
            {
                timer = 0;
                int n = Random.Range(1, m_FootstepSounds.Length);
                m_AudioSource.clip = m_FootstepSounds[n];
                m_AudioSource.PlayOneShot(m_AudioSource.clip);
                // move picked sound to index 0 so it's not picked next time
                m_FootstepSounds[n] = m_FootstepSounds[0];
                m_FootstepSounds[0] = m_AudioSource.clip;
            }
        }
        else
        {
            timer = 0;
        }

    }
}
