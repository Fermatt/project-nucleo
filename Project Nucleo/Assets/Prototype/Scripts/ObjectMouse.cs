﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ObjectMouse : MonoBehaviour
    , IPointerClickHandler
	// ... And many more available!
{

    public ItemData data;
    private Image img;

    private void Start()
    {
        img = gameObject.GetComponent<Image>();
        img.sprite = data.Avatar;
    }

    void Update()
    {
        if(Inventario.instance.selected == data)
        {
            img.color = new Color(255, 193, 193);
        }
        else
        {
            img.color = new Color(255, 255, 255);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Inventario.instance.selected = data;
    }


}
