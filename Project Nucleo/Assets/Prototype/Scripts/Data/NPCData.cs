﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/NPC")]
public class NPCData : ScriptableObject
{
    public int id;
    public string nombre;
    public Sprite Avatar;
    public Sprite RelationAvatar;
    public DialogueData[] dialogues;
    public DialogueData notItemDialogue;
}
