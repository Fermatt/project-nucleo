﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

[Serializable]
public class DialogueQuestion
{
	public string dialogue;
    public NPCData NPC;
    public DialogueResponse[] decisions;
	public EventData reward;
}

[Serializable]
public class DialogueResponse
{
    public string answer;
    public int relationNeeded;
    public DialogueData nextData;
    public DialogueQuestion nextDialogue;
	public bool oneTime;
}

[CreateAssetMenu(menuName = "Game/Dialog")]
public class DialogueData : ScriptableObject
{
	public string dialogue;
	public NPCData NPC;
    public DialogueResponse[] decisions;
	public EventData reward;
	public bool isNPC;
}
