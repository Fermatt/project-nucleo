﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/Event")]
public class EventData : ScriptableObject
{
    public NPCData npc;
    public ItemData item;
    public DeductionData deduction;
    public int[] relacionFoto;
    public int[] relacionHilo;
    public int relationship;
    public int tree;
    public int[] CV;
    public string changeScene;
    public bool destroyNPC;
    public float timerMax;
    public string sceneTimer;
    public int[] allTrees;

}
