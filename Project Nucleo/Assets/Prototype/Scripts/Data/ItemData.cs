﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ItemResponse
{
    public NPCData npc;
    public DialogueData answer;
}

[CreateAssetMenu(menuName = "Game/Item")]
public class ItemData : ScriptableObject
{
    public string Name;
    public Sprite Avatar;
    public string Descripcion;
    public ItemResponse[] itemDialogue;
    public int key;
}
