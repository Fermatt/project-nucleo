﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/Deduction")]
public class DeductionData : ScriptableObject {
	public bool deduc;
	public string deduction;
}
