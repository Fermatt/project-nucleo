﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DeductionTEXT : MonoBehaviour {

    public static DeductionTEXT instance;
    [SerializeField]
	private DeductionData []Deductions;
	public Text txt;
	void Start(){
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            gameObject.SetActive(true);
            transform.localScale = new Vector3(0, 0, 0);
        }
        else
        {
            Destroy(gameObject);
        }
        UpdateDeduction ();
	}
	public void UpdateDeduction () {
        txt.text = "";
		for (int b = 0; b < Deductions.Length; b++) {
            if (Deductions[b])
            {
                if (Deductions[b].deduc == true)
                {
                    txt.text += Deductions[b].deduction + "\n";
                }
            }
		}
	}

    public void addDeduction(DeductionData dd)
    {
        for (int i = 0; i < Deductions.Length; i++)
        {
            if (!Deductions[i])
            {
                Deductions[i] = dd;
                i = Deductions.Length;
            }
        }
        UpdateDeduction();
    }

    public void changeActive()
    {
        if (GameManager.instance.gameState == "Play")
        {
            GameManager.instance.gameState = "Deduction";
            transform.localScale = new Vector3(1, 1, 1);
        }
        else if (GameManager.instance.gameState == "Deduction")
        {
            GameManager.instance.gameState = "Play";
            transform.localScale = new Vector3(0, 0, 0);
        }
    }
}
