﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneOnTrigger : MonoBehaviour {

    [SerializeField]
    private string level;

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag == "Player")
        {
            DipToBlack.instance.dipToBlackScene(level);
        }
    }
}
