﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendDialogue : MonoBehaviour {

    [SerializeField] private GameObject inven;
    [SerializeField] private ItemData cancel;


    public void next()
    {
        bool b = false;
        ItemData id = Inventario.instance.selected;
        if (id != null)
        {
			if (id != cancel) {
				NPCData n = DialogueManager.instance.getCurrentNPC ();
				for (int i = 0; i < id.itemDialogue.Length; i++) {
					if (id.itemDialogue [i].npc == n) {
						b = true;
						DialogueManager.instance.StartDialogue (id.itemDialogue [i].answer);
						Inventario.instance.removeItem (id);
					}
				}
				if (!b) {
					
					DialogueManager.instance.StartDialogue (n.notItemDialogue);
				}

			} else {
				NPCData n = DialogueManager.instance.getCurrentNPC ();
				DialogueManager.instance.StartDialogue(n.notItemDialogue);
			}
			Inventario.instance.selected = null;
            inven.transform.localScale = new Vector3(0, 0, 0);
            Inventario.instance.setIsSelecting(false);

        }
    }
}
