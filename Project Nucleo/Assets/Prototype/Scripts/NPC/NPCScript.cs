﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCScript : MonoBehaviour {

    [SerializeField]
    private NPCData npc;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            Tutorial.instance.openTut2();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Tutorial.instance.closeTut();
        }
    }

    void OnTriggerStay2D(Collider2D c)
    {
        if (Input.GetKeyDown(KeyCode.E) && GameManager.instance.talkingNPC == null)
        {/*
			bool r = false;
			if (Inventario.instance.selected != null) {
				for (int i = 0; i < Inventario.instance.selected.itemDialogue.Length; i++)
					if (Inventario.instance.selected && npc.id == Inventario.instance.selected.itemDialogue [i].npc.id) {
						DialogueManager.instance.StartDialogue (Inventario.instance.selected.itemDialogue [i].answer);
						r = true;
					}
			}
            if (!r)
            {
                DialogueManager.instance.StartDialogue(npc.dialogues[GameManager.instance.treePos[npc.id]]);
                GameManager.instance.talkingNPC = gameObject;
            }*/
            Tutorial.instance.closeTut();
            DialogueManager.instance.StartDialogue(npc.dialogues[GameManager.instance.treePos[npc.id]]);
            GameManager.instance.talkingNPC = gameObject;
        }
    }
}
