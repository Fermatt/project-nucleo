﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventario : MonoBehaviour {

    public static Inventario instance;
	public ItemData[] Items;
    public ItemData selected;
    [SerializeField] private Text title;
    [SerializeField] private Text desc;
    public ItemData cancel;
    private bool isSelecting;
    [SerializeField] private GameObject next;


    private GameObject[] buttonObj;
    [SerializeField]
    private GameObject buttonPrefab;
    [SerializeField] private Transform Content;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            gameObject.SetActive(true);
            if (GameManager.instance.ItemsD != null)
            {
                Items = GameManager.instance.ItemsD;
            }
            updateInventory();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Update()
    {
        if(isSelecting)
        {
            next.transform.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            next.transform.localScale = new Vector3(0, 0, 0);
        }
        if(!selected || selected == cancel)
        {
            title.text = "";
            desc.text = "";
        }
        else
        {
            title.text = selected.name;
            desc.text = selected.Descripcion;
        }

    }

    void updateInventory()
    {
        GameManager.instance.ItemsD = Items;
        if(buttonObj != null)
        {
            for(int i = 0; i < buttonObj.Length; i++)
            {
                Destroy(buttonObj[i]);
            }
        }
        buttonObj = new GameObject[Items.Length];/*
        GameObject go = Instantiate(buttonPrefab);
        go.gameObject.transform.SetParent(Content, false);
        buttonObj[0] = go;
        buttonObj[0].gameObject.SetActive(true);*/
        for (int i = 0; i < GameManager.instance.ItemsD.Length; i++)
        {
            if (GameManager.instance.ItemsD[i])
            {
                GameObject go2 = Instantiate(buttonPrefab);
                go2.gameObject.transform.SetParent(Content, false);
                buttonObj[i] = go2;
                buttonObj[i].gameObject.SetActive(true);
                ObjectMouse om = buttonObj[i].GetComponent<ObjectMouse>();
                om.data = GameManager.instance.ItemsD[i];
            }
        }
    }

    public void addItem(ItemData id)
    {
        for(int i = 0; i < GameManager.instance.ItemsD.Length; i++)
        {
            if(!GameManager.instance.ItemsD[i])
            {
                GameManager.instance.ItemsD[i] = id;
                i = GameManager.instance.ItemsD.Length;
            }
        }
        updateInventory();
    }

    public void removeItem(ItemData id)
    {
        for (int i = 0; i < GameManager.instance.ItemsD.Length; i++)
        {
            if (GameManager.instance.ItemsD[i] == id)
            {
                GameManager.instance.ItemsD[i] = null;
                i = GameManager.instance.ItemsD.Length;
            }
        }
        updateInventory();
    }

    public void setIsSelecting(bool i)
    {
        isSelecting = i;
    }
}
