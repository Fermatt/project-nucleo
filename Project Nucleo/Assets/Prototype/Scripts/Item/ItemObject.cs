﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemObject : MonoBehaviour {

    private Sprite s;
    [SerializeField]
    int item;
    [SerializeField]
    string text;
    [SerializeField]
    private GameObject inventario;
    [SerializeField]
    private GameObject textBox;

    // Use this for initialization
    void Start () {
        s = gameObject.GetComponent<SpriteRenderer>().sprite;
	}

    void OnTriggerEnter2D(Collider2D c)
    {
        //GameManager.instance.Items[item] = true;
        GameManager.instance.gameState = "Text";
        Destroy(gameObject);
    }
}
