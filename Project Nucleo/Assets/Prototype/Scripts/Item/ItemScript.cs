﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemScript : MonoBehaviour {

    [SerializeField]
    private ItemData item;

    void OnTriggerStay2D(Collider2D c)
    {
        Inventario.instance.addItem(item);
        Destroy(gameObject);
    }
}
