﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemUpdate : MonoBehaviour {

    private Vector3 size;

	void Start () {
        size = transform.localScale;
        transform.localScale = new Vector3(0, 0, 0);
	}

	// Update is called once per frame
	void Update () {

	}

	public void ShowItem()
	{
		if (GameManager.instance.gameState == "Play")
		{
			GameManager.instance.gameState = "Inventario";
            transform.localScale = size;
			Inventario.instance.selected = null;
        }
		else if (GameManager.instance.gameState == "Inventario")
		{
			GameManager.instance.gameState = "Play";
			transform.localScale = new Vector3(0, 0, 0);
			Inventario.instance.selected = null;
		}
	}
}

