﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GAMEWIN : MonoBehaviour {

	public Image UIImage;
	void Start () {
		UIImage = GameObject.Find ("Image").GetComponent<Image> ();
        UIImage.sprite = Resources.Load<Sprite>("Game/gameOver");
    }
}
