﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CVTextHistoria : MonoBehaviour {

    private string ultimoNombre;
    public Text txt;
    private string[] Nombre;
    void Start()
    {

    }

    // Update is called once per frame + "\n"+ "hola" txt.text =
    void Update()
    {
        Nombre = new string[9];
        texto(ultimoNombre);
    }

    private void OnEnable()
    {
        texto(ultimoNombre);
    }

    public void texto(string nombre)
    {
        ultimoNombre = nombre;

        if (nombre == "Dina")
        {
            if (GameManager.instance.CVname[1][8] == false)
            {
                Nombre[0] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[0] = "Es profesora en una escuela primaria";
            }
            if(GameManager.instance.CVname[1][9] == false)
            {
                Nombre[1] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[1] = "Su hija está enferma y se encuentra hospitalizada";
            }
            if(GameManager.instance.CVname[1][10] == false)
            {
                Nombre[2] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[2] = "Está escribiendo un libro infantil que espera poder publicar para pagar las cuentas del hospital";
            }
            if (GameManager.instance.CVname[1][11] == false)
            {
                Nombre[3] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[3] = "Su hija suele estar bajo vigilancia las 24 horas, pero en ocasiones le permiten estar unos días en su casa";
            }
            if (GameManager.instance.CVname[1][12] == false)
            {
                Nombre[4] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[4] = "Un dia la hija sufrió un ataque, en el apuro camino al hospital atropelló a una joven, abandonando la escena";
            }
            if (GameManager.instance.CVname[1][13] == false)
            {
                Nombre[5] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[5] = "Su auto era un reno 6 azul";
            }
            txt.text = Nombre[0] + "\n" + Nombre[1] + "\n" + Nombre[2] + "\n" + Nombre[3] + "\n" + Nombre[4] + "\n" + Nombre[5];
        }
        if (nombre == "Eugene")
        {
            if (GameManager.instance.CVname[2][9] == false)
            {
                Nombre[0] = "███████ █████ ██████ ";
            }
            else
            {
                Nombre[0] = "No tiene amigos";
            }
            if (GameManager.instance.CVname[2][10] == false)
            {
                Nombre[1] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[1] = "Le gusta ver cómo las personas se relacionan pero le asusta relacionarse";
            }
            if (GameManager.instance.CVname[2][11] == false)
            {
                Nombre[2] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[2] = "Tiene la costumbre de grabar a las personas";
            }
            if (GameManager.instance.CVname[2][12] == false)
            {
                Nombre[3] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[3] = "Grabo a Blake, Christie y Abby en repetidas ocasiones";
            }
            if (GameManager.instance.CVname[2][13] == false)
            {
                Nombre[4] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[4] = "Está obsesionado con Abby y su vida";
            }
            if (GameManager.instance.CVname[2][14] == false)
            {
                Nombre[5] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[5] = "Fue testigo cuando un auto atropelló a una chica";
            }
            if (GameManager.instance.CVname[2][15] == false)
            {
                Nombre[6] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[6] = "Escapó de la escena sin llamar a nadie";
            }
            if (GameManager.instance.CVname[2][16] == false)
            {
                Nombre[7] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[7] = "La chica que fue atropellada era Abby";
            }
            if (GameManager.instance.CVname[2][17] == false)
            {
                Nombre[8] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[8] = "Era un auto azul";
            }
            txt.text = Nombre[0] + "\n" + Nombre[1] + "\n" + Nombre[2] + "\n" + Nombre[3] + "\n" + Nombre[4] + "\n" + Nombre[5] + "\n" + Nombre[6] + "\n" + Nombre[7] + "\n" + Nombre[8];
        }
        if (nombre == "Christie")
        {
            if (GameManager.instance.CVname[3][9] == false)
            {
                Nombre[0] = "███████ █████ ██████ ";
            }
            else
            {
                Nombre[0] = "Cuando tenía 16 años, el trastorno histérico de Christie alcanzó su punto más alto luego de la ruptura con su novio y por ello intentó suicidarse";
            }
            if (GameManager.instance.CVname[3][10] == false)
            {
                Nombre[1] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[1] = "Pero con la ayuda de su mejor amiga Abby, pudo recuperarse emocionalmente poco a poco";
            }
            if (GameManager.instance.CVname[3][11] == false)
            {
                Nombre[2] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[2] = "Para su último año de preparatoria, Christie tenía muchos amigos y era querida por muchos pero al mismo tiempo odiada por otros debido a su frialdad y superficialidad hacia los que ella consideraba inferiores";
            }
            if (GameManager.instance.CVname[3][12] == false)
            {
                Nombre[3] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[3] = "Abby no destacaba mucho, pero sus amistades eran genuinas y ese talento musical que la sacaría del pueblo lentamente provocaron envidia en Christie";
            }
            if (GameManager.instance.CVname[3][13] == false)
            {
                Nombre[4] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[4] = "Christie no podía tolerar ser menos que su “amiga”, por eso en la última fiesta de preparatoria se acostó con el novio de abby y dejo que está los encontrara, con el único fin de alimentar su deseo de superioridad, esa noche abby murió en un accidente";
            }
            
            txt.text = Nombre[0] + "\n" + Nombre[1] + "\n" + Nombre[2] + "\n" + Nombre[3] + "\n" + Nombre[4];
        }
        if (nombre == "Hammond")
        {
            if (GameManager.instance.CVname[4][9] == false)
            {
                Nombre[0] = "███████ █████ ██████ ";
            }
            else
            {
                Nombre[0] = "Una noche le reportaron un incidente con una chica que fue atropellada";
            }
            if (GameManager.instance.CVname[4][10] == false)
            {
                Nombre[1] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[1] = "Durante la investigación descubrió que está chica era compañera de su hija y que su hija estuvo relacionada con la muerte de la chica";
            }
            if (GameManager.instance.CVname[4][11] == false)
            {
                Nombre[2] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[2] = "Para evitar pasar una humillación a su hija y teniendo en cuenta su trastorno, cerró la investigación por falta de pruebas";
            }
            txt.text = Nombre[0] + "\n" + Nombre[1] + "\n" + Nombre[2];
        }
        if (nombre == "Blake")
        {
            if (GameManager.instance.CVname[5][9] == false)
            {
                Nombre[0] = "███████ █████ ██████ ";
            }
            else
            {
                Nombre[0] = "Muy popular en la preparatoria, buena relación con todos a su alrededor";
            }
            if (GameManager.instance.CVname[5][10] == false)
            {
                Nombre[1] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[1] = "No pudo jugar la final de basket por una lesión";
            }
            if (GameManager.instance.CVname[5][11] == false)
            {
                Nombre[2] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[2] = "La lesión no sano en su totalidad impidiendo que siga en el basket";
            }
            if (GameManager.instance.CVname[5][12] == false)
            {
                Nombre[3] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[3] = "Tenía una beca en la misma ciudad donde se mudaba su novia, pero perdió la beca por no podes seguir jugando";
            }
            if (GameManager.instance.CVname[5][13] == false)
            {
                Nombre[4] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[4] = "Sus bajas calificaciones no le permitían aplicar a ninguna universidad fuera de su pueblo";
            }
            if (GameManager.instance.CVname[5][14] == false)
            {
                Nombre[5] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[5] = "Lo invadió una depresión y el constante éxito de su novia solo lo enfurecia";
            }
            if (GameManager.instance.CVname[5][15] == false)
            {
                Nombre[6] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[6] = "Engañó a su novia para lastimarla";
            }
            txt.text = Nombre[0] + "\n" + Nombre[1] + "\n" + Nombre[2] + "\n" + Nombre[3] + "\n" + Nombre[4] + "\n" + Nombre[5] + "\n" + Nombre[6];
        }
        if (nombre == "Frank")
        {
            if (GameManager.instance.CVname[6][8] == false)
            {
                Nombre[0] = "███████ █████ ██████ ";
            }
            else
            {
                Nombre[0] = "Tuvo algún problema con su esposa";
            }
            if (GameManager.instance.CVname[6][9] == false)
            {
                Nombre[1] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[1] = "Estaba muy estresado en el trabajo";
            }
            if (GameManager.instance.CVname[6][10] == false)
            {
                Nombre[2] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[2] = "No podía tener hijos. Este fue el motivo de su estrés";
            }
            if (GameManager.instance.CVname[6][11] == false)
            {
                Nombre[3] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[3] = "Un dia el estrés lo desbordó, dejó su turno unos minutos para ir a tomar unos tragos";
            }
            if (GameManager.instance.CVname[6][12] == false)
            {
                Nombre[4] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[4] = "Recibió una llamada de emergencia mientras tomaba, una chica tuvo un accidente y tenía que ser operada de urgencia";
            }
            if (GameManager.instance.CVname[6][13] == false)
            {
                Nombre[5] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[5] = "Como no había otro cirujano no dijo nada de su estado y operó bajo los efectos del alcohol, la chica murió por un error de Frank ";
            }
            txt.text = Nombre[0] + "\n" + Nombre[1] + "\n" + Nombre[2] + "\n" + Nombre[3] + "\n" + Nombre[4] + "\n" + Nombre[5];
        }
        if (nombre == "William")
        {
            if (GameManager.instance.CVname[7][9] == false)
            {
                Nombre[0] = "███████ █████ ██████ ";
            }
            else
            {
                Nombre[0] = "Jugó ajedrez profesionalmente";
            }
            if (GameManager.instance.CVname[7][10] == false)
            {
                Nombre[1] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[1] = "Ganó muchos torneos y ahora entrena futuros jugadores";
            }
            if (GameManager.instance.CVname[7][11] == false)
            {
                Nombre[2] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[2] = "Trota de 9 a 11 todos los días, en las afueras de la ciudad";
            }
            if (GameManager.instance.CVname[7][12] == false)
            {
                Nombre[3] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[3] = "Da clases en el centro de 11:00 a 13:00";
            }
            if (GameManager.instance.CVname[7][13] == false)
            {
                Nombre[4] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[4] = "No tiene auto se maneja a pata para todos lados";
            }
            if (GameManager.instance.CVname[7][14] == false)
            {
                Nombre[5] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[5] = "No tiene hijos";
            }
            if (GameManager.instance.CVname[7][15] == false)
            {
                Nombre[6] = "█████████ ████████ ██████ ████ █████ █████";
            }
            else
            {
                Nombre[6] = "Le molesta que le pregunten porque no tiene hijos";
            }
            txt.text = Nombre[0] + "\n" + Nombre[1] + "\n" + Nombre[2] + "\n" + Nombre[3] + "\n" + Nombre[4] + "\n" + Nombre[5] + "\n" + Nombre[6];
        }
        if (nombre == "-")
        {
            txt.text = "";
        }
    }
}
