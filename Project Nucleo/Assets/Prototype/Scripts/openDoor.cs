﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class openDoor : MonoBehaviour {

    [SerializeField] private BoxCollider2D bc;
    [SerializeField] private int Llave;
    [SerializeField] private Sprite openDoorSprite;
    [SerializeField] private AudioClip open;
    [SerializeField] private AudioClip locked;
    [SerializeField] private AudioSource aud;
    private SpriteRenderer door;

    private void Start()
    {
        door = gameObject.GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            if (bc.enabled)
            {
                if (Llave < 0)
                {
                    bc.enabled = false;
                    door.sprite = openDoorSprite;
                    aud.clip = open;
                    aud.Play();
                }
                else if (GameManager.instance.Llaves[Llave])
                {
                    bc.enabled = false;
                    door.sprite = openDoorSprite;
                    aud.clip = open;
                    aud.Play();
                }
                else
                {
                    aud.clip = locked;
                    aud.Play();
                }
            }
        }
    }
}
