﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	public static GameManager instance;
	public string gameState;
    public int[] treePos;
	public int[] relationship;
	public int MaxPersonajes;
	public int MaxCV;
    public bool alive;
	public bool[][] CVname;
    public ItemData[] ItemsD;
	public bool[] Hilos;// controla los hilos que estan funcionando
	public bool[] Foto;// contola las fotos
    public bool[] Llaves;
    public bool[][] ScriptDialogues;

    public GameObject talkingNPC;

    public bool restart;

	void Awake()
	{
        if (!restart)
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }
        else
        {
            if(instance != null)
                Destroy(instance.gameObject);
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
		CVname = new bool [MaxPersonajes][];
        ScriptDialogues = new bool[MaxPersonajes][];
        relationship = new int [MaxPersonajes];
		for (int i = 0; i < relationship.Length; i++)
			relationship [i] = 0;
        ItemsD = new ItemData[15];
        for(int i = 0; i < ItemsD.Length; i++)
        {
            ItemsD[i] = null;
        }
        
        treePos = new int[MaxPersonajes];
		for(int a = 0;a < MaxPersonajes;a++){
            ScriptDialogues[a] = new bool[10];
			CVname [a] = new bool[MaxCV];
            for (int b = 0; b < CVname[a].Length; b++)
            {
                if(b < ScriptDialogues[a].Length)
                    ScriptDialogues[a][b] = false;
                CVname[a][b] = false;
            }
            treePos[a] = 0;
		}
		for(int c = 0; c < Hilos.Length; c++){
			Hilos[c] = false;
		}
		for(int d = 0; d < Foto.Length; d++){
			Foto [d] = false;
		}
        Llaves = new bool[5];
        for (int e = 0; e < Llaves.Length; e++)
        {
            Llaves[e] = false;
        }
    }

    private void Start()
    {
        //ItemsD[0] = Inventario.instance.cancel;
    }

    public void LoadLevel(string level)
    {
        SceneManager.LoadScene(level);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
