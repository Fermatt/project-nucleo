﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenClose : MonoBehaviour {
    
    public bool down = false;
    public Animator anim;

    public void Update()
    {
        anim.SetBool("down", down);
    }

    public void moveUp()
    {
        down = false;
    }

    public void moveDown()
    {
        down = true;
    }
}
