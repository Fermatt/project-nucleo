﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour {

	[SerializeField] private int velocity;
	private Rigidbody2D rb;
    private AudioSource aud;
	private Animator anim;

	// Use this for initialization
	void Start () {
		rb = gameObject.GetComponent<Rigidbody2D> ();
        GameManager.instance.gameState = "Play";
        aud = gameObject.GetComponent<AudioSource>();
		anim = GetComponent<Animator> ();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (GameManager.instance.gameState == "Play")
        {
            float i = Input.GetAxis("Vertical");
            float k = Input.GetAxis("Horizontal");
            Vector2 v = transform.up * i + transform.right * k;
            v *= velocity;
            //rb.position += v;
            rb.velocity = v;
            anim.SetFloat("Vertical", i);
            anim.SetFloat("Horizontal", k);
			/*
			if (i > 0) {
				anim.SetFloat ("Vertical", i);			
			} 
			else if (i < 0) {
				anim.SetFloat ("Vartical", i);
			}
			else {
				anim.SetFloat ("Horizontal", k);	
			}*/
        }
        else
        {
            rb.velocity = Vector3.zero;
            anim.SetFloat("Vertical", 0);
            anim.SetFloat("Horizontal", 0);
        }
	}
}
