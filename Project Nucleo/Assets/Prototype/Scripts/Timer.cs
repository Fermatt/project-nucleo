﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    static public Timer instance;
    [SerializeField] private Image timerImage;
    [SerializeField] private Text timerText;
    private float timer;
    private bool running;
    private string nextScene;

    private void Awake()
    {
        if(!instance)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void setNextScene(string s)
    {
        nextScene = s;
    }

    public void setRunning(bool b)
    {
        running = b;
    }

    public void setMaxTime(float i)
    {
        timer = i;
    }

    public bool getRunning()
    {
        return running;
    }

    private void Start()
    {
        running = false;
        timer = 0;
        nextScene = "";
    }

    private void Update()
    {
        if(running)
        {
            timerImage.enabled = true;
            timerText.enabled = true;
            timer -= Time.deltaTime;
            timerText.text = "Tiempo - " + (int)timer / 60 + ":" + (int)timer % 60;
            if(timer < 0)
            {
                SceneManager.LoadScene(nextScene);
                running = false;
            }
        }
        else
        {
            timerText.text = "";
            timerImage.enabled = false;
            timerText.enabled = false;
            
        }
    }
}
