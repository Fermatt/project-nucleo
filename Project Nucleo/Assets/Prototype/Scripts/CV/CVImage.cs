﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CVImage : MonoBehaviour {

	public Image UIImage;
    [SerializeField] private Sprite main;

    // Use this for initialization
    void Start () {
		UIImage.sprite = main;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
	public void CambioImagen(int a)
	{
		if (a == 0) {
			UIImage.sprite = Resources.Load<Sprite> ("FotosCV/Dina1");
		}
		else if (a == 1) {
			UIImage.sprite = Resources.Load<Sprite> ("FotosCV/Eugene1");
		}
        else if (a == 2) {
			UIImage.sprite = Resources.Load<Sprite> ("FotosCV/Cristie1");
		}
        else if (a == 3)
        {
            UIImage.sprite = Resources.Load<Sprite>("FotosCV/Hammond1");
        }
        else if (a == 4)
        {
            UIImage.sprite = Resources.Load<Sprite>("FotosCV/Blake1");
        }
        else if (a == 5)
        {
            UIImage.sprite = Resources.Load<Sprite>("FotosCV/Frank1");
        }
        else if (a == 6)
        {
            UIImage.sprite = Resources.Load<Sprite>("FotosCV/William1");
        }
        else
        {
            UIImage.sprite = Resources.Load<Sprite>("FotosCV/Sola");
        }
    }
}
