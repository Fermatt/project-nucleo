﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CVUpdate : MonoBehaviour {

    private Vector3 size;

    // Use this for initialization
    void Start () {
        size = transform.localScale;
        transform.localScale = new Vector3(0, 0, 0);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowCV()
    {
        if (GameManager.instance.gameState == "Play")
        {
            GameManager.instance.gameState = "CV";
            transform.localScale = size;
            
        }
        else if (GameManager.instance.gameState == "CV")
        {
            GameManager.instance.gameState = "Play";
            transform.localScale = new Vector3(0, 0, 0);
        }
    }
}
