﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CVTextNombre : MonoBehaviour {

    private string ultimoNombre;
	public Text[] txt;
	private string [] Nombre;
	void Start () {

	}

	// Update is called once per frame + "\n"+ "hola" txt.text =
	void Update () {
		Nombre = new string[5];
        texto(ultimoNombre);
    }

    private void OnEnable()
    {
        texto(ultimoNombre);
    }

    public void texto (string nombre){
        ultimoNombre = nombre;

        if (nombre == "Dina") {
			if (GameManager.instance.CVname[1][0] == false) {
				Nombre[0] = "█████████";
			}
			else {
				Nombre[0] = "Dina";
			}
            txt[0].text = Nombre[0];
			if (GameManager.instance.CVname[1][1] == false) {
				Nombre[1] = "██";
			}
			else {
				Nombre[1] = "28";
			}
            txt[1].text = Nombre[1];
            if (GameManager.instance.CVname[1][2] == false) {
				Nombre[2] = "████";
			}
			else {
				Nombre[2] = "1,68";
			}
            txt[2].text = Nombre[2];
            if (GameManager.instance.CVname[1][3] == false) {
				Nombre[3] = "████████";
			}
			else {
				Nombre[3] = "Femenino";
			}
            txt[3].text = Nombre[3];
            if (GameManager.instance.CVname[1][4] == false) {
				Nombre[4] = "██████";
			}
			else {
				Nombre[4] = "Diestra";
			}
            txt[4].text = Nombre[4];
        }
		if (nombre == "Eugene") {
			if (GameManager.instance.CVname[2][0] == false) {
				Nombre[0] = "█████";
			}
			else {
				Nombre[0] = "Eugene";
			}
            txt[0].text = Nombre[0];
            if (GameManager.instance.CVname[2][1] == false) {
				Nombre[1] = "██";
			}
			else {
				Nombre[1] = "18";
			}
            txt[1].text = Nombre[1];
            if (GameManager.instance.CVname[2][2] == false) {
				Nombre[2] = "████";
			}
			else {
				Nombre[2] = "1,60";
			}
            txt[2].text = Nombre[2];
            if (GameManager.instance.CVname[2][3] == false) {
				Nombre[3] = "█████████";
			}
			else {
				Nombre[3] = "Masculino";
			}
            txt[3].text = Nombre[3];
            if (GameManager.instance.CVname[2][4] == false) {
				Nombre[4] = "██████";
			}
			else {
				Nombre[4] = "Zurdo";
			}
            txt[4].text = Nombre[4];
        }
        if (nombre == "Christie")
        {
            if (GameManager.instance.CVname[3][0] == false)
            {
                Nombre[0] = "███";
            }
            else
            {
                Nombre[0] = "Christie";
            }
            if (GameManager.instance.CVname[3][1] == false)
            {
                Nombre[1] = "██";
            }
            else
            {
                Nombre[1] = "18";
            }
            if (GameManager.instance.CVname[3][2] == false)
            {
                Nombre[2] = "████";
            }
            else
            {
                Nombre[2] = "1,68";
            }
            if (GameManager.instance.CVname[3][3] == false)
            {
                Nombre[3] = "█████████";
            }
            else
            {
                Nombre[3] = "Femenino";
            }
            if (GameManager.instance.CVname[3][4] == false)
            {
                Nombre[4] = "██████";
            }
            else
            {
                Nombre[4] = "Diestro";
            }
            txt[0].text = Nombre[0];
            txt[1].text = Nombre[1];
            txt[2].text = Nombre[2];
            txt[3].text = Nombre[3];
            txt[4].text = Nombre[4];
        }
        if (nombre == "Hammond")
        {
            if (GameManager.instance.CVname[4][0] == false)
            {
                Nombre[0] = "███";
            }
            else
            {
                Nombre[0] = "Hammond";
            }
            if (GameManager.instance.CVname[4][1] == false)
            {
                Nombre[1] = "██";
            }
            else
            {
                Nombre[1] = "44";
            }
            if (GameManager.instance.CVname[4][2] == false)
            {
                Nombre[2] = "████";
            }
            else
            {
                Nombre[2] = "1,75";
            }
            if (GameManager.instance.CVname[4][3] == false)
            {
                Nombre[3] = "█████████";
            }
            else
            {
                Nombre[3] = "Masculino";
            }
            if (GameManager.instance.CVname[4][4] == false)
            {
                Nombre[4] = "██████";
            }
            else
            {
                Nombre[4] = "Diestro";
            }
            txt[0].text = Nombre[0];
            txt[1].text = Nombre[1];
            txt[2].text = Nombre[2];
            txt[3].text = Nombre[3];
            txt[4].text = Nombre[4];
        }
        if (nombre == "Blake")
        {
            if (GameManager.instance.CVname[5][0] == false)
            {
                Nombre[0] = "███";
            }
            else
            {
                Nombre[0] = "Blake";
            }
            if (GameManager.instance.CVname[5][1] == false)
            {
                Nombre[1] = "██";
            }
            else
            {
                Nombre[1] = "18";
            }
            if (GameManager.instance.CVname[5][2] == false)
            {
                Nombre[2] = "████";
            }
            else
            {
                Nombre[2] = "1,85";
            }
            if (GameManager.instance.CVname[5][3] == false)
            {
                Nombre[3] = "█████████";
            }
            else
            {
                Nombre[3] = "Masculino";
            }
            if (GameManager.instance.CVname[5][4] == false)
            {
                Nombre[4] = "██████";
            }
            else
            {
                Nombre[4] = "Diestro";
            }
            txt[0].text = Nombre[0];
            txt[1].text = Nombre[1];
            txt[2].text = Nombre[2];
            txt[3].text = Nombre[3];
            txt[4].text = Nombre[4];
        }
        if (nombre == "Frank")
        {
            if (GameManager.instance.CVname[6][0] == false)
            {
                Nombre[0] = "███";
            }
            else
            {
                Nombre[0] = "Frank";
            }
            if (GameManager.instance.CVname[6][1] == false)
            {
                Nombre[1] = "██";
            }
            else
            {
                Nombre[1] = "36";
            }
            if (GameManager.instance.CVname[6][2] == false)
            {
                Nombre[2] = "████";
            }
            else
            {
                Nombre[2] = "1,80";
            }
            if (GameManager.instance.CVname[6][3] == false)
            {
                Nombre[3] = "█████████";
            }
            else
            {
                Nombre[3] = "Masculino";
            }
            if (GameManager.instance.CVname[6][4] == false)
            {
                Nombre[4] = "██████";
            }
            else
            {
                Nombre[4] = "Zurdo";
            }
            txt[0].text = Nombre[0];
            txt[1].text = Nombre[1];
            txt[2].text = Nombre[2];
            txt[3].text = Nombre[3];
            txt[4].text = Nombre[4];
        }
        if (nombre == "William")
        {
            if (GameManager.instance.CVname[7][0] == false)
            {
                Nombre[0] = "███";
            }
            else
            {
                Nombre[0] = "William";
            }
            if (GameManager.instance.CVname[7][1] == false)
            {
                Nombre[1] = "██";
            }
            else
            {
                Nombre[1] = "40";
            }
            if (GameManager.instance.CVname[7][2] == false)
            {
                Nombre[2] = "████";
            }
            else
            {
                Nombre[2] = "1,60";
            }
            if (GameManager.instance.CVname[7][3] == false)
            {
                Nombre[3] = "█████████";
            }
            else
            {
                Nombre[3] = "Masculino";
            }
            if (GameManager.instance.CVname[7][4] == false)
            {
                Nombre[4] = "██████";
            }
            else
            {
                Nombre[4] = "???????";
            }
            txt[0].text = Nombre[0];
            txt[1].text = Nombre[1];
            txt[2].text = Nombre[2];
            txt[3].text = Nombre[3];
            txt[4].text = Nombre[4];
        }
        if (nombre == "-")
        {
            txt[0].text = "";
            txt[1].text = "";
            txt[2].text = "";
            txt[3].text = "";
            txt[4].text = "";
        }
    }
}
