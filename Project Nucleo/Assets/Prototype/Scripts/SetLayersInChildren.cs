﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetLayersInChildren : MonoBehaviour {

    public int Layer;
    public string LayerName;
    private SpriteRenderer[] sr;

    // Use this for initialization
    void Start()
    {
        sr = GetComponentsInChildren<SpriteRenderer>();

    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < sr.Length; i++)
        {
            sr[i].sortingOrder = Layer;
            sr[i].sortingLayerName = LayerName;
        }

    }
}
