﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DipToBlack : MonoBehaviour {

    static public DipToBlack instance;
    private bool activeZero;
    private bool activeOne;
    private string scene;
    private Vector3 position;
    private GameObject objToMove;
    [SerializeField] private Image img;
    [SerializeField] private float velocity;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);
        }
    }

    // Update is called once per frame
    void Update () {
		if(activeZero)
        {
            img.color = img.color + new Color(0,0,0,velocity);
            if(img.color.a >= 1)
            {
                if(scene != null)
                {
                    SceneManager.LoadScene(scene);
                }
                else
                {
                    objToMove.transform.position = position;
                    objToMove = null;
                }
                activeZero = false;
                activeOne = true;
            }
        }
        else if (activeOne)
        {
            img.color = img.color - new Color(0, 0, 0, velocity);
            if (img.color.a <= 0)
            {
                activeOne = false;
                GameManager.instance.gameState = "Play";
            }
        }
	}

    public void dipToBlackPosition(Vector3 pos, GameObject obj)
    {
        GameManager.instance.gameState = "DipToBlack";
        if (pos != Vector3.zero && obj != null)
        {
            objToMove = obj;
            position = pos;
            scene = null;
            activeZero = true;
        }
    }

    public void dipToBlackScene(string _scene)
    {
        GameManager.instance.gameState = "DipToBlack";
        if (_scene != null)
        {
            scene = _scene;
            position = Vector3.zero;
            activeZero = true;
        }
    }
}
