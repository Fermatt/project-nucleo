﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RelacionUpdate : MonoBehaviour {

    private Vector3 size;

    void Start()
    {
        size = transform.localScale;
        transform.localScale = new Vector3(0, 0, 0);
    }

    void Update()
    {

    }

    public void ShowRel()
    {
        if (GameManager.instance.gameState == "Play")
        {
            GameManager.instance.gameState = "Relation";
            transform.localScale = size;
        }
        else if (GameManager.instance.gameState == "Relation")
        {
            GameManager.instance.gameState = "Play";
            transform.localScale = new Vector3(0, 0, 0);
        }
    }
}
