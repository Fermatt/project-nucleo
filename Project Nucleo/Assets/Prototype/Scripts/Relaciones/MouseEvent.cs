﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MouseEvent : MonoBehaviour 
	, IPointerEnterHandler
	, IPointerExitHandler
	// ... And many more available!
{
	
	public Text txt;
	public string desc;
    public int a;

	public void OnPointerEnter(PointerEventData eventData)
	{
        if(GameManager.instance.Hilos[a])
		    txt.text = desc;
	}
	public void OnPointerExit(PointerEventData eventData)
	{
		txt.text = "";
	}
}

	
