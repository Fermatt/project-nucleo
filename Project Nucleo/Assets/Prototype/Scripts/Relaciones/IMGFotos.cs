﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IMGFotos : MonoBehaviour {

	public NPCData npc;
    [SerializeField] private Image FotoImage;
	public string name;
	public int CodigoFoto;//numero unico identificador de cada foto
	void Start () {
	}
	void Update () {
		if (GameManager.instance.Foto[CodigoFoto] == true)
        {
			FotoImage.sprite = npc.RelationAvatar;
		}
        else
        {
            FotoImage.sprite = Resources.Load<Sprite>("Relaciones/foto");
        }
    }
}
