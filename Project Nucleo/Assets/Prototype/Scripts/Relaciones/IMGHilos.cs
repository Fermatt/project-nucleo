﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IMGHilos: MonoBehaviour {

	public Image DeduImage;
    public Sprite img;
	public string name;
	public int CodigoHilo;//numero unico identificador de cada hilo
	void Start () {
		DeduImage = gameObject.GetComponent<Image> ();
	}
	void Update () {
		if (GameManager.instance.Hilos[CodigoHilo] == true)
        {
			DeduImage.sprite = img;
		}
        else
        {
            DeduImage.sprite = Resources.Load<Sprite>("Relaciones/hilo_0");
        }
    }
}
