﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartingDialogue : MonoBehaviour {

    [SerializeField] private DialogueData d;


	// Use this for initialization
	void Start () {
        DialogueManager.instance.StartDialogue(d);
	}
}
