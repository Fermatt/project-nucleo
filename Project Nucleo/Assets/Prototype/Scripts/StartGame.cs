﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour {

	public void LoadLevel(string level)
	{
        DipToBlack.instance.dipToBlackScene(level);
	}

	public void ExitGame()
	{
		Application.Quit();
	}
}
