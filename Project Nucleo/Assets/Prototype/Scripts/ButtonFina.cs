﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonFina : MonoBehaviour {

	// Use this for initialization
	void Start () {
        gameObject.transform.localScale = new Vector3(0, 0, 0);
	}
	
	// Update is called once per frame
	void Update () {
		if(Timer.instance.getRunning() && SceneManager.GetActiveScene().name == "Mansion")
        {
            gameObject.transform.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            gameObject.transform.localScale = new Vector3(0, 0, 0);
        }
	}

    public void buttonPressed()
    {
		if (GameManager.instance.gameState == "Play") {
			Timer.instance.setRunning (false);
			SceneManager.LoadScene ("DecisionFinal");
		}
    }
}
