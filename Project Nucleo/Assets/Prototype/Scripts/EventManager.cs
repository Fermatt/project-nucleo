﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EventManager : MonoBehaviour {

    public static EventManager instance;


    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void runEvent(EventData e)
    {
        if(e.item)
        {
            if (e.item.key < 0)
                Inventario.instance.addItem(e.item);
            else
                GameManager.instance.Llaves[e.item.key] = true;
        }
        if (e.deduction)
        {
            DeductionTEXT.instance.addDeduction(e.deduction);
        }
		for (int i = 0; i < e.relacionFoto.Length; i++) {
			if (e.relacionFoto[i] > 0) {
				GameManager.instance.Foto [e.relacionFoto[i]] = true;
                InterfaceUpdate.instance.showUpdate();

            }
        }
		for (int i = 0; i < e.relacionHilo.Length; i++) {
			if (e.relacionHilo[i] > 0) {
				GameManager.instance.Hilos [e.relacionHilo[i] - 1] = true;
                InterfaceUpdate.instance.showUpdate();

            }
        }
        if (e.relationship > 0 || e.relationship < 0)
        {
			GameManager.instance.relationship[e.npc.id] += e.relationship;
        }
        if (e.tree > GameManager.instance.treePos[e.npc.id])
        {
            GameManager.instance.treePos[e.npc.id] = e.tree;
        }
		for (int i = 0; i < e.CV.Length; i++) {
			if (e.CV[i] > 0) {
				GameManager.instance.CVname [e.npc.id] [e.CV[i] - 1] = true;
                InterfaceUpdate.instance.showUpdate();
			}
		}
        if(e.changeScene != "")
        {
            DipToBlack.instance.dipToBlackScene(e.changeScene);
        }
        if(e.timerMax >0 && e.sceneTimer != "")
        {
            Timer.instance.setMaxTime(e.timerMax);
            Timer.instance.setNextScene(e.sceneTimer);
            Timer.instance.setRunning(true);
        }
        if(e.destroyNPC)
        {
            Destroy(GameManager.instance.talkingNPC);
        }
        for (int i = 0; i < e.allTrees.Length; i++)
        {
            if (e.allTrees[i] > 0 && GameManager.instance.treePos[i+1] < e.allTrees[i])
            {
                GameManager.instance.treePos[i+1] = e.allTrees[i];
            }
        }

    }
}
