﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public static DialogueManager instance;
    public Text dialogueText;
	public Text nameText;
	public Image avatar;
    private GameObject[] buttonObj;
    [SerializeField]
    private GameObject buttonPrefab;
    [SerializeField] private Transform Content;
	public int offsetButton;
    [SerializeField] private GameObject inven;

    private bool active;

    private DialogueData currentDialogue;
	private DialogueQuestion currentQuestion;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            gameObject.SetActive(false);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if(gameObject.activeInHierarchy)
        {
            GameManager.instance.gameState = "Text";
        }
    }

    public NPCData getCurrentNPC()
    {
        if (currentDialogue != null)
        {
            return currentDialogue.NPC;
        }
        else if (currentQuestion != null)
        {
            return currentQuestion.NPC;
        }
        else
            return null;
    }

    public void StartDialogue(DialogueData dialogue)
    {
        GameManager.instance.gameState = "Text";
        currentDialogue = dialogue;
		currentQuestion = null;
		if(currentDialogue != null && currentDialogue.dialogue != "")
        {
			if (currentDialogue.isNPC)
				buttonObj = new GameObject[dialogue.decisions.Length + 2];
			else
				buttonObj = new GameObject[dialogue.decisions.Length];
            GameManager.instance.gameState = "Text";
            gameObject.SetActive(true);
            dialogueText.text = dialogue.dialogue;
			nameText.text = dialogue.NPC.nombre;
			avatar.sprite = dialogue.NPC.Avatar;
            for (int i = 0; i < dialogue.decisions.Length; i++)
            {
                if (GameManager.instance.relationship[dialogue.NPC.id] >= dialogue.decisions[i].relationNeeded && (!GameManager.instance.ScriptDialogues[dialogue.NPC.id][i] || !dialogue.decisions[i].oneTime))
                {
                    GameObject go = Instantiate(buttonPrefab);
                    go.gameObject.transform.SetParent(Content, false);
                    buttonObj[i] = go;
                    buttonObj[i].gameObject.SetActive(true);
                    buttonObj[i].GetComponentInChildren<Text>().text = dialogue.decisions[i].answer;
                    int n = i;
                    buttonObj[i].GetComponent<Button>().onClick.AddListener(delegate { SelectDialogue(n); });
                }
                else if((!GameManager.instance.ScriptDialogues[dialogue.NPC.id][i] || !dialogue.decisions[i].oneTime))
                {
                    GameObject go = Instantiate(buttonPrefab);
                    go.gameObject.transform.SetParent(Content, false);
                    buttonObj[i] = go;
                    buttonObj[i].gameObject.SetActive(true);
                    buttonObj[i].GetComponentInChildren<Text>().text = "??????";
                    //int n = i;
                    //buttonObj[i].GetComponent<Button>().onClick.AddListener(delegate { SelectDialogue(n); });
                }
            }
			if (currentDialogue.isNPC) {/*
				GameObject go = Instantiate(buttonPrefab);
				go.gameObject.transform.SetParent(Content, false);
				buttonObj[dialogue.decisions.Length] = go;
				buttonObj[dialogue.decisions.Length].gameObject.SetActive(true);
				buttonObj[dialogue.decisions.Length].GetComponentInChildren<Text>().text = "Item";
                buttonObj[dialogue.decisions.Length].GetComponent<Button>().onClick.AddListener(delegate { showInven(); });
                */
                GameObject go2 = Instantiate(buttonPrefab);
				go2.gameObject.transform.SetParent(Content, false);
				buttonObj[dialogue.decisions.Length] = go2;
				buttonObj[dialogue.decisions.Length].gameObject.SetActive(true);
				buttonObj[dialogue.decisions.Length].GetComponentInChildren<Text>().text = "Adios";
				int n = dialogue.decisions.Length;
				buttonObj[dialogue.decisions.Length].GetComponent<Button>().onClick.AddListener( delegate { SelectDialogue(n); });
			}
        }
        else
        {
            GameManager.instance.gameState = "Play";
            gameObject.SetActive(false);
            GameManager.instance.talkingNPC = null;
        }
    }

	public void StartDialogue(DialogueQuestion dialogue)
	{
        GameManager.instance.gameState = "Text";
        currentQuestion = dialogue;
		currentDialogue = null;
		if(currentQuestion != null && currentQuestion.dialogue != "")
		{
			buttonObj = new GameObject[dialogue.decisions.Length];
			GameManager.instance.gameState = "Text";
			gameObject.SetActive(true);
			dialogueText.text = dialogue.dialogue;
            nameText.text = dialogue.NPC.nombre;
            avatar.sprite = dialogue.NPC.Avatar;
            for (int i = 0; i < dialogue.decisions.Length; i++)
            {
                if (GameManager.instance.relationship[dialogue.NPC.id] >= dialogue.decisions[i].relationNeeded)
                {
                    GameObject go = Instantiate(buttonPrefab);
                    go.gameObject.transform.SetParent(Content, false);
                    buttonObj[i] = go;
                    buttonObj[i].gameObject.SetActive(true);
                    buttonObj[i].GetComponentInChildren<Text>().text = dialogue.decisions[i].answer;
                    int n = i;
                    buttonObj[i].GetComponent<Button>().onClick.AddListener(delegate { SelectDialogue(n); });
                }
                else
                {
                    GameObject go = Instantiate(buttonPrefab);
                    go.gameObject.transform.SetParent(Content, false);
                    buttonObj[i] = go;
                    buttonObj[i].gameObject.SetActive(true);
                    buttonObj[i].GetComponentInChildren<Text>().text = "??????";
                    //int n = i;
                    //buttonObj[i].GetComponent<Button>().onClick.AddListener(delegate { SelectDialogue(n); });
                }
			}
		}
		else
		{
			GameManager.instance.gameState = "Play";
			gameObject.SetActive(false);
            GameManager.instance.talkingNPC = null;
        }
	}

    public void showInven()
    {
        for (int j = 0; j < buttonObj.Length; j++)
        {
            Destroy(buttonObj[j]);
        }
        inven.transform.localScale = new Vector3(1, 1, 1);
        Inventario.instance.setIsSelecting(true);
    }

    public void SelectDialogue(int i)
    {
        for (int j = 0; j < buttonObj.Length; j++)
        {
            Destroy(buttonObj[j]);
        }
		if (currentQuestion == null) {
			
			if (currentDialogue.reward != null) {
				EventManager.instance.runEvent (currentDialogue.reward);
			}
			if (i < currentDialogue.decisions.Length) {
				if (currentDialogue.decisions [i].oneTime) {
                    GameManager.instance.ScriptDialogues[currentDialogue.NPC.id][i] = true;
				}
                if(currentDialogue.decisions[i].nextData)
                {
                    StartDialogue(currentDialogue.decisions[i].nextData);
                }else
				    StartDialogue (currentDialogue.decisions [i].nextDialogue);
			} else {
				GameManager.instance.gameState = "Play";
				gameObject.SetActive (false);
                GameManager.instance.talkingNPC = null;
            }
		} else {
			
			if (currentQuestion.reward != null)
			{
				EventManager.instance.runEvent(currentQuestion.reward);
			}
            if (i < currentQuestion.decisions.Length)
            {
				if (currentQuestion.decisions [i].oneTime) {
                    GameManager.instance.ScriptDialogues[currentQuestion.NPC.id][i] = true;
                }
                if (currentQuestion.decisions[i].nextData)
                {
                    StartDialogue(currentQuestion.decisions[i].nextData);
                }
                else
                    StartDialogue(currentQuestion.decisions[i].nextDialogue);
            }
            else {
				GameManager.instance.gameState = "Play";
				gameObject.SetActive(false);
                GameManager.instance.talkingNPC = null;
			}
		}

    }
}
