﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecuerdoImage : MonoBehaviour {

    private Image UIImage;
    [SerializeField]
    private Sprite[] images;
    private bool[] BoolRecuerdo;
    private int maxChar;

    // Use this for initialization
    void Start () {
        UIImage = gameObject.GetComponent<Image>();
        maxChar = images.Length;
        BoolRecuerdo = new bool[maxChar];
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CambioImagen(int a)
    {
        UIImage.sprite = images[a];
    }
}