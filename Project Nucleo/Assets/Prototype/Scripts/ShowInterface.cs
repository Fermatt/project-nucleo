﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ShowInterface : MonoBehaviour
    , IPointerEnterHandler
// ... And many more available!
{

    [SerializeField] private GameObject panel;
    private OpenClose oc;
    [SerializeField] private bool down;

    private void Start()
    {
        oc = panel.GetComponent<OpenClose>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (GameManager.instance.gameState == "Play")
        {
            if (down)
            {
                oc.moveDown();
                InterfaceUpdate.instance.hideUpdate();
            }
            else
                oc.moveUp();
        }
    }
}
