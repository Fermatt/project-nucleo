﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerTransport : MonoBehaviour {

    [SerializeField]
    private GameObject transport;
    [SerializeField] private bool door;
    [SerializeField] private int Llave = -1;

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag == "Player")
        {
            if(door)
            {
                if (Llave < 0)
                {
                    DipToBlack.instance.dipToBlackPosition(transport.transform.position, c.gameObject);
                }
                else if (GameManager.instance.Llaves[Llave])
                {
                    DipToBlack.instance.dipToBlackPosition(transport.transform.position, c.gameObject);
                }
                else
                {

                }
            }
            else if(transport != null)
                DipToBlack.instance.dipToBlackPosition(transport.transform.position, c.gameObject);
        }
    }
}
