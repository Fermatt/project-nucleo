﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Credits : MonoBehaviour {
    // Update is called once per frame
    [SerializeField] private int velocity;
    [SerializeField] private GameObject ini;
    [SerializeField] private GameObject end;

    void Update () {
        transform.Translate(0, velocity * Time.deltaTime, 0);
        if(ini.transform.position.y > end.transform.position.y || Input.GetButtonDown("Fire1"))
        {
            SceneManager.LoadScene("Menu");
        }
	}
}
