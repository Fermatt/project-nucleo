﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class activatebutton : MonoBehaviour {

    [SerializeField] private Sprite idle;
    [SerializeField] private Sprite active;
    [SerializeField] private Image img;
    [SerializeField] private Button button;
    [SerializeField] private int id;

    private void Start()
    {
        img.sprite = idle;
        button.enabled = false;
    }

    private void Update()
    {
        if(GameManager.instance.CVname[id][0])
        {
            img.sprite = active;
            button.enabled = true;
        }
        else
        {
            img.sprite = idle;
            button.enabled = false;
        }
    }
}
