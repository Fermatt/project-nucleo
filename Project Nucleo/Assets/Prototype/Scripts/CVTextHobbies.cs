﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CVTextHobbies : MonoBehaviour {

    private string ultimoNombre;
    public Text txt;
    private string[] Nombre;
    void Start()
    {

    }
    //txt.text = Nombre[0] + "\n" + Nombre[1] + "\n" + Nombre[2] + "\n" + Nombre[3] + "\n" + Nombre[4];
    // Update is called once per frame + "\n"+ "hola" txt.text =
    void Update()
    {
        Nombre = new string[5];
        texto(ultimoNombre);
    }

    private void OnEnable()
    {
        texto(ultimoNombre);
    }

    public void texto(string nombre)
    {
        ultimoNombre = nombre;

        if (nombre == "Dina")
        {
            if (GameManager.instance.CVname[1][7] == false)
            {
                Nombre[0] = "█████████ █████████ █████████";
            }
            else
            {
                Nombre[0] = "Escribir cuentos infantiles";
            }
            txt.text = Nombre[0];
        }
        if (nombre == "Eugene")
        {
            if (GameManager.instance.CVname[2][7] == false)
            {
                Nombre[0] = "█████████ █████████ █████████";
            }
            else
            {
                Nombre[0] = "Pasar horas navegando en internet";
            }
            if (GameManager.instance.CVname[2][8] == false)
            {
                Nombre[1] = "█████████ █████████ █████████";
            }
            else
            {
                Nombre[1] = "Investigar conspiraciones del gobierno";
            }
            txt.text = Nombre[0] + "\n" + Nombre[1];
        }
        if (nombre == "Christie")
        {
            if (GameManager.instance.CVname[3][7] == false)
            {
                Nombre[0] = "█████████ █████████ █████████";
            }
            else
            {
                Nombre[0] = "Salir con sus amigos";
            }
            if (GameManager.instance.CVname[3][8] == false)
            {
                Nombre[1] = "█████████ █████████ █████████";
            }
            else
            {
                Nombre[1] = "Ir de compras";
            }
            txt.text = Nombre[0] + "\n" + Nombre[1];
        }
        if (nombre == "Hammond")
        {
            if (GameManager.instance.CVname[4][7] == false)
            {
                Nombre[0] = "█████████ █████████ █████████";
            }
            else
            {
                Nombre[0] = "Tocar la armónica";
            }
            if (GameManager.instance.CVname[4][8] == false)
            {
                Nombre[1] = "████████ ████ ";
            }
            else
            {
                Nombre[1] = "Ver TV";
            }
            txt.text = Nombre[0] + "\n" + Nombre[1];
        }
        if (nombre == "Blake")
        {
            if (GameManager.instance.CVname[5][7] == false)
            {
                Nombre[0] = "█████████ █████████ █████████";
            }
            else
            {
                Nombre[0] = "Jugar Basket";
            }
            if (GameManager.instance.CVname[5][8] == false)
            {
                Nombre[1] = "████████ ████ ";
            }
            else
            {
                Nombre[1] = "Tocar la guitarra";
            }
            txt.text = Nombre[0] + "\n" + Nombre[1];
        }
        if (nombre == "Frank")
        {
            if (GameManager.instance.CVname[6][7] == false)
            {
                Nombre[0] = "█████████ █████████ █████████";
            }
            else
            {
                Nombre[0] = "Escuchar música clásica ";
            }
            txt.text = Nombre[0];
        }
        if (nombre == "William")
        {
            if (GameManager.instance.CVname[7][7] == false)
            {
                Nombre[0] = "█████████ █████████ █████████";
            }
            else
            {
                Nombre[0] = "Jugar ajedrez";
            }
            if (GameManager.instance.CVname[7][8] == false)
            {
                Nombre[1] = "████████ ████ ";
            }
            else
            {
                Nombre[1] = "Trotar en las mañanas";
            }
            txt.text = Nombre[0] + "\n" + Nombre[1];
        }
        if (nombre == "-")
        {
            txt.text = "";
        }

    }
}
