﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour {

    static public Tutorial instance;
    [SerializeField] private Sprite tut1;
    [SerializeField] private Sprite tut2;
    [SerializeField] private Sprite tut3;
    [SerializeField] private SpriteRenderer sr;


    // Use this for initialization
    void Awake () {
		if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
	}

    public void closeTut()
    {
        sr.sprite = null;
    }

    public void openTut1()
    {
        sr.sprite = tut1;
    }

    public void openTut2()
    {
        sr.sprite = tut2;
    }

    public void openTut3()
    {
        sr.sprite = tut3;
    }
}
