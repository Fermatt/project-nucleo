﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterfaceUpdate : MonoBehaviour {

    static public InterfaceUpdate instance;

	// Use this for initialization
	void Awake () {
		if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        hideUpdate();
	}
	
	public void showUpdate()
    {
        gameObject.SetActive(true);
    }

    public void hideUpdate()
    {
        gameObject.SetActive(false);
    }
}
